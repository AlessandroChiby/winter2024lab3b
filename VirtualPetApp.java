public class VirtualPetApp {
    public static void main(String[] args) {
        java.util.Scanner reader = new java.util.Scanner(System.in);
        Parrot[] flockOfParrots = new Parrot[4];
        String name;
        String color;
        String species;
        for (int i = 0; i < flockOfParrots.length; i++) {
            System.out.println("Bird " + (i + 1) + "/" + flockOfParrots.length);
            System.out.println("Enter name:");
            name = reader.next();
            System.out.println("Enter color:");
            color = reader.next();
            System.out.println("Enter species:");
            species = reader.next();
            flockOfParrots[i] = new Parrot(name, color, species);
            System.out.println();
        }
        //Getting attributes of LAST animal
        System.out.println(flockOfParrots[flockOfParrots.length - 1].name);
        System.out.println(flockOfParrots[flockOfParrots.length - 1].color);
        System.out.println(flockOfParrots[flockOfParrots.length - 1].species);

        //Calling the 2 instance methods for FIRST animal
        flockOfParrots[0].speak();
        flockOfParrots[0].molt();
    }
}