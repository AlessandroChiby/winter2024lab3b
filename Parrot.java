public class Parrot {
    public String name;
    public String color;
    public String species;

    public Parrot(String name, String color, String species) {
        this.name = name;
        this.color = color;
        this.species = species;
    }
    public void speak() {
        System.out.println("*squawk* My name is " + name + ".");
    }
    public void molt() {
        System.out.println("*squawk* There are " + color.toLowerCase() + " feathers all over the place! *squawk*");
    }
}